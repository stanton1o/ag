#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <memory>
#include <limits>
#include <optional>
#include <algorithm>
#include <bitset>
#include <list>
#include <array>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <stack>
#include <queue>

using Place = size_t;

struct Map {
    size_t places;
    Place start, end;
    std::vector<std::pair<Place, Place>> connections;
    std::vector<std::vector<Place>> items;
};

template < typename F, typename S >
struct std::hash<std::pair<F, S>> {
    std::size_t operator () (const std::pair<F, S> &p) const noexcept {
        // something like boost::combine would be much better
        return std::hash<F>()(p.first) ^ (std::hash<S>()(p.second) << 1);
    }
};

#endif
using namespace std;
using Kod = std::size_t;

list<Place> reconstructPath(const std::map<pair<Kod, Place>, pair<Kod, Place>>& parent, Place currentPlace, Kod currentKod){
    list<Place> res {currentPlace};

    auto it = parent.find(make_pair(currentKod, currentPlace)) ;
    while( it != parent.end() ){
        res.push_front(it->second.second);
        currentKod = it->second.first;
        currentPlace = it->second.second;
        it = parent.find(make_pair(currentKod, currentPlace));
    }
    return res;
}

Kod makeKod(const set<Kod> &val) {
    Kod kod = 0;
    for( auto i : val){
        size_t res = 1;
        res <<= i;
        kod |= res;
    }

    return kod;
}

void create_graf(std::multimap<Place, Place> &graf, const std::vector<std::pair<Place, Place>> &connections){
    for( const auto &[key, val] : connections){
        graf.insert({key, val});
        graf.insert({val, key});
    }
}

//ДЛя быстро доступа к инофрмации и типе подарка.
bool create_groups(std::map<Place, set<Kod>> &groups, const std::vector<std::vector<Place>> &items){
    size_t count = 0;

    for(const auto &i : items)
        if ( i.empty() )
            return false;
        else{
            ++count;
            for( auto j : i)
                groups[j].insert(count);
        }
    return true;
}

void create_start_finish_kod(Kod& finishKod, Kod& startKod, size_t size, const set<Kod> & items){
    set<size_t> tmp;
    for( size_t i = 1 ; i <= size; ++i){
        tmp.insert(i);
    }
    finishKod = makeKod(tmp);
    startKod = makeKod(items);
}
std::list<Place> find_path(const Map &map) {
    std::multimap<Place, Place> graf;
    std::map<Kod, set<Place>> visited;
    std::queue<pair<Kod, Place>> mQueue;
    std::map<Place, set<Kod>> groups;
    std::map<pair<Kod, Place>, pair<Kod, Place>> parent;

    if(!create_groups(groups, map.items))
        return initializer_list<Place>{};
    create_graf(graf, map.connections);

    Kod finishKod, startKod;
    create_start_finish_kod(finishKod, startKod, map.items.size(), groups[map.start]);

    if(!map.connections.empty())
        mQueue.push(make_pair(startKod, map.start));
    visited[startKod].insert(map.start);

    while(!mQueue.empty()){
        auto [currentKod, currentPlace] = mQueue.front();
        mQueue.pop();

        //Если все собрали и в конечном узле.
        if(currentKod == finishKod && currentPlace == map.end){
            return  reconstructPath(parent, map.end, finishKod);
        }

        //Найти всех соседей.
        auto [nf, nl] = graf.equal_range(currentPlace);
        for( auto act = nf ; act != nl ; ++act){
            //Если не был посещен.
            if (visited[currentKod].insert(act->second).second){
                Kod newKod = currentKod;
                //Если данная комбинация (собранные подарки + узел) еще не открыта
                if ( !groups[act->second].empty() && visited[newKod |= makeKod(groups[act->second])].insert(act->second).second){
                    //перейти на новый уровень и добавить в очередь
                    mQueue.push(make_pair(newKod, act->second));
                    parent[{newKod, act->second}]= {currentKod, currentPlace};
                }
                //Иначе добавить соседа в очередь и продолжить
                else{
                    mQueue.push(make_pair(currentKod, act->second));
                    parent[{currentKod, act->second}]= {currentKod, currentPlace};
                }
            }
        }
    }
    return initializer_list<Place>{};
}

#ifndef __PROGTEST__

using TestCase = std::pair<size_t, Map>;

// Class template argument deduction exists since C++17 :-)
const std::array examples = {
        TestCase{ 1, Map{ 2, 0, 0,
                          { { 0, 1 } },
                          { { 0 } }
        }},
        TestCase{ 3, Map{ 2, 0, 0,
                          { { 0, 1 } },
                          { { 1 } }
        }},
        TestCase{ 3, Map{ 4, 0, 1,
                          { { 0, 2 }, { 2, 3 }, { 0, 3 }, { 3, 1 } },
                          {}
        }},
        TestCase{ 4, Map{ 4, 0, 1,
                          { { 0, 2 }, { 2, 3 }, { 0, 3 }, { 3, 1 } },
                          { { 2 } }
        }},
        TestCase{ 0, Map{ 4, 0, 1,
                          { { 0, 2 }, { 2, 3 }, { 0, 3 }, { 3, 1 } },
                          { { 2 }, {} }
        }},
};

int main() {
    int fail = 0;
    for (size_t i = 0; i < examples.size(); i++) {
        auto sol = find_path(examples[i].second);
        if (sol.size() != examples[i].first) {
            std::cout << "Wrong anwer for map " << i << std::endl;
            fail++;
        }else{
            cout << "Test " << i << " completed" << endl;
        }
    }

    if (fail) std::cout << "Failed " << fail << " tests" << std::endl;
    else std::cout << "All tests completed" << std::endl;

    return 0;
}

#endif
