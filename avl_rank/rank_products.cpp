#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <memory>
#include <limits>
#include <optional>
#include <algorithm>
#include <bitset>
#include <list>
#include <array>
#include <vector>
#include <deque>
#include <unordered_set>
#include <unordered_map>
#include <stack>
#include <queue>
#include <random>

#endif

using namespace std;
template<typename T, typename C>
class AvlTree {
private:
    struct Node {
        T key;
        C val;
        Node *parent;
        //Общее кол-во проданных продуктов слева
        size_t amountL;
        //Общее кол-во проданных продуктов справа
        size_t amountR;
        //Общее кол-во типов проданных продуктов слева
        size_t numL;
        //Общее кол-во типов проданных продуктов справа
        size_t numR;
        //Глубина дерева справа
        int Rh;
        //Глубина дерева слева
        int Lh;
        //Глубина дерева в текущем узле
        int H;
        Node *R;
        Node *L;

        Node(T nkey, C nval, Node *par = nullptr, size_t amountl = 0, size_t amountr = 0, size_t numl = 0, size_t numr = 0,  int rh = 0, int lh = 0, int h = 1,
             Node *r = nullptr,
             Node *l = nullptr) :
                key(std::move(nkey)), val(nval), parent(par),  amountL(amountl), amountR(amountr), numL(numl), numR(numr), Rh(rh), Lh(lh), H(h), R(r), L(l) {}
    };

    Node *root;
    size_t size;

    static int heightAvlTree(const Node *node) {
        if (!node)
            return 0;
        return node->H;
    }

    void updateHeight(Node *&node) {
        node->Lh = heightAvlTree(node->L);
        node->Rh = heightAvlTree(node->R);
        node->H = node->Lh > node->Rh ? node->Lh + 1 : node->Rh + 1;
    }

    Node *rightRotation(Node *node) {
        Node *nRoot = node->L;
        Node *LRT = nRoot->R;

        if (node == root)
            root = nRoot;

        nRoot->R = node;
        node->L = LRT;
        if (LRT)
            LRT->parent = nRoot->parent;

        nRoot->parent = node->parent;
        node->parent = nRoot;

        node->numL = nRoot->numR;
        nRoot->numR = node->numL + node->numR + 1;

        node->amountL = nRoot->amountR;
        nRoot->amountR = node->amountL + node->amountR + node->val;

        updateHeight(node);
        updateHeight(nRoot);

        return nRoot;
    }

    Node *leftRotation(Node *node) {
        Node *nRoot = node->R;
        Node *LRT = nRoot->L;

        if (node == root)
            root = nRoot;

        nRoot->L = node;
        node->R = LRT;
        if (LRT)
            LRT->parent = nRoot->parent;
        nRoot->parent = node->parent;
        node->parent = nRoot;

        node->numR = nRoot->numL;
        nRoot->numL = node->numL + node->numR + 1;

        node->amountR = nRoot->amountL;
        nRoot->amountL = node->amountL + node->amountR + node->val;

        updateHeight(node);
        updateHeight(nRoot);

        return nRoot;
    }

    // Проверка баланса и его регулировка
    void updateTree(Node *&node, int relation) {
        //Если перевес справа
        if (relation > 1) {
            int relR = node->R->Rh - node->R->Lh;
            // Если перевес внутри правого дерева сколяется налево
            if (relR < 0) {
                node->R = rightRotation(node->R);
                node = leftRotation(node);
            } else if (relR >= 0) {
                node = leftRotation(node);
            }
            // Если перевес слева
        } else if (relation < -1) {
            int relL = node->L->Rh - node->L->Lh;
            if (relL <= 0) {
                node = rightRotation(node);
                 // Если перевес внутри левого дерева сколяется направо
            } else if (relL > 0) {
                node->L = leftRotation(node->L);
                node = rightRotation(node);
            }
        }
    }

    Node *copyAvlTree(Node *node, Node *par = nullptr) {
        if (node) {
            Node *tmp = new Node(node->key, node->val, par, node->Rh, node->Lh, node->H);
            tmp->L = copyAvlTree(node->L, tmp);
            tmp->R = copyAvlTree(node->R, tmp);
            return tmp;
        }
        return nullptr;
    }

public:
    AvlTree() : root(nullptr), size(0) {}

    ~AvlTree() {
        destroyTree(root);
        root = nullptr;
    }

    AvlTree(const AvlTree &avlTree) {
        root = copyAvlTree(avlTree.root);
    }

    AvlTree operator=(const AvlTree &avlTree) {
        root = copyAvlTree(avlTree.root);
    }

    Node *findMin(Node *node) const {
        auto it = node;
        if (!it)
            return it;

        while (it->L)
            it = it->L;

        return it;
    }


    void destroyTree(Node *&node) {

        if (node) {
            destroyTree(node->L);
            destroyTree(node->R);
            delete node;
            node = nullptr;
        }
    }

    size_t getSize() const {
        return size;
    }

    Node *getRoot() const {
        return root;
    }

    // Для реализации некоторых методов ниже, но перестал быть нужен, после добавление доп информации в узел.
    class Iterator {
        Node *ptr;
    public:
        Iterator(Node *node) : ptr(node) {}

        const Node *operator*() {
            return ptr;
        }

//        const T getKey() const {
//            return ptr->key;
//        }

        const C getVal() const {
            return ptr->val;
        }

        Node *getPtr() const {
            return ptr;
        }

        void setVal(const C &val) {
            ptr->val = val;
        }

        Iterator operator++(int) {
            Iterator it(ptr);
            ++(*this);
            return it;
        }

        Iterator &operator++() {
            if (!ptr)
                return *this;
            if (ptr->R) {

                ptr = ptr->R;
                while (ptr->L) {
                    ptr = ptr->L;
                }
            } else {
                Node *tmp = ptr->parent;

                while (tmp && tmp->key < ptr->key) {
                    ptr = tmp;
                    tmp = tmp->parent;
                }
                ptr = tmp;
            }
            return *this;
        }

        Iterator operator--(int) {
            Iterator it(ptr);
            --(*this);
            return it;
        }

        Iterator &operator--() {
            if (!ptr)
                return *this;
            if (ptr->L) {

                ptr = ptr->L;
                while (ptr->R) {
                    ptr = ptr->R;
                }
            } else {
                Node *tmp = ptr->parent;

                while (tmp && tmp->key > ptr->key) {
                    ptr = tmp;
                    tmp = tmp->parent;
                }
                ptr = tmp;
            }
            return *this;
        }

        friend bool operator>(const Iterator &iter1, const Iterator &iter2) {
            return iter1.ptr->key > iter2.ptr->key;
        }

        friend bool operator<(const Iterator &iter1, const Iterator &iter2) {
            return iter1.ptr->key < iter2.ptr->key;
        }

        friend bool operator==(const Iterator &iter1, const Iterator &iter2) {
            return iter1.ptr == iter2.ptr;
        }

        friend bool operator!=(const Iterator &iter1, const Iterator &iter2) {
            return !(iter1 == iter2);
        }
    };

    Iterator begin() {
        return Iterator(findMin(root));
    }


    Iterator end() const {
        return Iterator(nullptr);
    }

    Node *erase(Node *node, T key, C val) {

        if (!node)
            return node;

        if (key < node->key) {
            //Миунс 1 продукт слева
            --(node->numL);
            //Минус все проданные товары этого типа слева
            node->amountL -= val;
            node->L = erase(node->L, key, val);
        } else if (key > node->key) {
            //Миунс 1 продукт справа
            --(node->numR);
             //Минус все проданные товары этого типа справа
            node->amountR -= val;
            node->R = erase(node->R, key, val);
        } else {
            //Обычно удаление из дерева и 3 кейса: есть 2 сына, есть 1 сын, нет детей + Обновление информации
            if (node->R && node->L) {
                Node *tmp = findMin(node->R);
                node->key = tmp->key;
                node->val = tmp->val;
                --(node->numR);
                node->amountR -= tmp->val;
                node->R = erase(node->R, node->key, tmp->val);
            } else {
                Node *tmp;

                if (node->L) {
                    tmp = node->L;
                    node->key = tmp->key;
                    node->val = tmp->val;
                    node->numL = tmp->numL;
                    node->amountL = tmp->amountL;
                    node->L = node->R = nullptr;
                } else if (node->R) {
                    tmp = node->R;
                    node->key = tmp->key;
                    node->val = tmp->val;
                    node->numR = tmp->numR;
                    node->amountR = tmp->amountL;
                    node->L = node->R = nullptr;
                } else {
                    tmp = node;
                    tmp->parent = node->parent;
                    if (node == root)
                        root = nullptr;
                    node = nullptr;
                    if (tmp->parent) {
                        if (tmp->parent->R == tmp) {
                            tmp->parent->numR = 0;
                            tmp->parent->amountR = 0;
                            tmp->parent->R = nullptr;
                        } else {
                            tmp->parent->numL = 0;
                            tmp->parent->amountL = 0;
                            tmp->parent->L = nullptr;
                        }
                    }
                }
                --size;
                delete tmp;
            }
        }

        if (!node)
            return node;

        updateHeight(node);
        updateTree(node, node->Rh - node->Lh);
        if (!node->parent)
            root = node;
        return node;
    }

    Node *insert(Node *node, T key, C val, Node *par = nullptr) {

        if (!node) {
            ++size;
            if (!root)
                return root = new Node(key, val);
            return new Node(key, val, par);
        } else {

            if (node->key > key) {
                //Добаляем информацию о новом продукте и его кол-ве в левом дереве.
                ++(node->numL);
                node->amountL += val;
                node->L = insert(node->L, key, val, node);
            } else if (node->key < key) {
                //Аналогично справа
                ++(node->numR);
                node->amountR += val;
                node->R = insert(node->R, key, val, node);
            } else
                return node;
        }

        updateHeight(node);
        updateTree(node, node->Rh - node->Lh);

        return node;
    }

    Iterator find(const T &fkey) const {
        auto it = root;
        while (it) {
            if (it->key == fkey)
                return it;
            if (it->key > fkey)
                it = it->L;
            else
                it = it->R;
        }
        return nullptr;
    }

    // Мы можем легко вычислить ранк, если знаем сколько элементов (типов продуктов меньше, чем у нас)
    Iterator findProductByRank(size_t rank) const {
        auto it = root;
        size_t nodeRank = size;
        while (it) {
            if (rank > nodeRank - it->numL)
                it = it->L;
            else if (rank < nodeRank - it->numL) {
                nodeRank -= (it->numL + 1);
                it = it->R;
            } else
                return it;
        }
        return nullptr;
    }

    
    void findFirstSameRank(Node* node, size_t nodeRank ,size_t rank, size_t amount, size_t& sameRank) const {

        if(!node)
            return ;
        //Если нашли, то понижаем ранг и ищем в правом дерере дальше (тк там все ранги ниже)
        if(node->val == amount && rank >= nodeRank - node->numL){
            sameRank = nodeRank - node->numL;
            findFirstSameRank(node->R, nodeRank - node->numL - 1, rank, amount, sameRank);
        }else if (rank > nodeRank - node->numL)
            findFirstSameRank(node->L, nodeRank, rank, amount, sameRank);
        else //if(rank < nodeRank - node->numL)
            findFirstSameRank(node->R, nodeRank - node->numL - 1, rank, amount, sameRank);
    }

    void findLastSameRank(Node* node, size_t nodeRank ,size_t rank, size_t amount, size_t& sameRank) const {

        if(!node)
            return ;
        //Если нашли, то понижаем ранг и ищем в левом дерере дальше (тк там все ранги выше)
        if(node->val == amount && rank <= nodeRank - node->numL){
            sameRank = nodeRank - node->numL;
            findLastSameRank(node->L, nodeRank, rank, amount, sameRank);
        }else if (rank > nodeRank - node->numL)
            findLastSameRank(node->L, nodeRank, rank, amount, sameRank);
        else //if(rank < nodeRank - node->numL)
            findLastSameRank(node->R, nodeRank - node->numL - 1, rank, amount, sameRank);
    }

    size_t findTotalAmountByRank(size_t rank, bool includeVal = true) const {
        auto it = root;
        size_t nodeRank = size;
        size_t totalAmount = 0;
        while (it) {
            if (rank > nodeRank - it->numL)
                it = it->L;
            else if (rank < nodeRank - it->numL) {
                nodeRank -= (it->numL + 1);
                totalAmount += it->amountL + it->val;
                it = it->R;
            } else
                return includeVal ? totalAmount + it->amountL + it->val : totalAmount + it->amountL;
        }
        return -1;
    }


    size_t findRankByAmount(const T &key) const {
        size_t rank = size;
        auto node = root;
        while (node) {
            if (key < node->key)
                node = node->L;
            else if (key > node->key) {
                rank -= (node->numL + 1);
                node = node->R;
            } else
                return rank - node->numL;
        }
        return 0;
    }
};
template<typename Product>
struct Bestsellers {
private:
    AvlTree<Product, size_t> productDB;
    AvlTree<pair<size_t, Product>, size_t> rankDB;

public:
    // The total number of tracked products
    size_t products() const {
        return productDB.getSize();
    }

    void sell(const Product &p, size_t amount) {
        auto itProduct = productDB.find(p);
        if (itProduct != productDB.end()) {
            rankDB.erase(rankDB.getRoot(), {itProduct.getVal(), p}, itProduct.getVal());
            amount += itProduct.getVal();
            itProduct.setVal(amount);
            rankDB.insert(rankDB.getRoot(), {amount, p}, amount);
        } else {
            productDB.insert(productDB.getRoot(), p, amount);
            rankDB.insert(rankDB.getRoot(), {amount, p}, amount);
        }
    }

    // The most sold product has rank 1
    size_t rank(const Product &p) const {
        auto itProduct = productDB.find(p);
        if (itProduct == productDB.end())
            throw std::out_of_range("error rank");
        return rankDB.findRankByAmount({itProduct.getVal(), p});
    }

    const Product &product(size_t rank) const {
        auto itRank = rankDB.findProductByRank(rank);
        if (rank == 0 || rank > rankDB.getSize() || itRank == rankDB.end())
            throw std::out_of_range("error product");
        return itRank.getPtr()->key.second;
    }


    // How many copies of product with given rank were sold
    size_t sold(size_t rank) const {
        auto itRank = rankDB.findProductByRank(rank);
        if (rank == 0 || rank > rankDB.getSize() || itRank == rankDB.end())
            throw std::out_of_range("error sold(rank)");
        return itRank.getPtr()->key.first;
    }

    // The same but sum over interval of products (including from and to)
    // It must hold: sold(x) == sold(x, x)
    size_t sold(size_t from, size_t to) const {
        if (from > to || from == 0 || to > rankDB.getSize())
            throw std::out_of_range("error sold(from, to)");
        return rankDB.findTotalAmountByRank(from) - rankDB.findTotalAmountByRank(to, false);
    }

    //ищем инофрмацию о НИМЕНЬШЕМ ранге r_min для которого верно утверждение r.amount == r_min.amount
    size_t first_same(size_t r) const {
        if ( r == 0 || r > rankDB.getSize())
            throw std::out_of_range("first_same");
        auto itRank = rankDB.findProductByRank(r);
        size_t sameRank = r;
        rankDB.findFirstSameRank(rankDB.getRoot(), rankDB.getSize(), r, itRank.getVal(), sameRank);
        return sameRank;
    }

    //ищем инофрмацию о НАИБОЛЬШЕМ ранге r_mMAX для которого верно утверждение r.amount == r_max.amount
    size_t last_same(size_t r) const {
        if ( r == 0 || r > rankDB.getSize())
            throw std::out_of_range("first_same");
        auto itRank = rankDB.findProductByRank(r);
        size_t sameRank = r;
        rankDB.findLastSameRank(rankDB.getRoot(), rankDB.getSize(), r, itRank.getVal(), sameRank);
        return sameRank;
    }
};

#ifndef __PROGTEST__

void test1() {
    Bestsellers<std::string> T;
    T.sell("coke", 32);
    T.sell("bread", 1);
    assert(T.products() == 2);
    T.sell("ham", 2);
    T.sell("mushrooms", 12);

    assert(T.products() == 4);
    assert(T.rank("ham") == 3);
    assert(T.rank("coke") == 1);
    assert(T.sold(1, 3) == 46);
    assert(T.product(2) == "mushrooms");

    T.sell("ham", 11);
    assert(T.products() == 4);
    assert(T.product(2) == "ham");
    assert(T.sold(2) == 13);
    assert(T.sold(2, 2) == 13);
    assert(T.sold(1, 2) == 45);
}

void test2() {
# define CATCH(expr) \
  try { expr; assert(0); } catch (const std::out_of_range&) { assert(1); };

    Bestsellers<std::string> T;
    T.sell("coke", 32);
    T.sell("bread", 1);

    CATCH(T.rank("ham"));
    CATCH(T.product(3));
    CATCH(T.sold(0));
    CATCH(T.sold(9));
    CATCH(T.sold(0, 1));
    CATCH(T.sold(3, 2));
    CATCH(T.sold(1, 9));

#undef CATCH
}

int main() {
    test1();
    test2();
}

#endif



